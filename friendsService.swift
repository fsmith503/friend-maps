//
//  friendsService.swift
//  Friend Maps
//
//  Created by  Franklin Smith on 10/15/18.
//  Copyright © 2018  Franklin Smith. All rights reserved.
//

import Foundation
import  Firebase
import  FirebaseDatabase
class friendsService {
    
    static let shared = friendsService()
    
    public static var friendsList : Array<Any> = []
    public static var ref: DatabaseReference!
    public init() {
        friendsService.ref.child("users").child("profile").observeSingleEvent(of: .value, with: { (snapshot) in
                for users in snapshot.children.allObjects as! [DataSnapshot] {
                    guard let profileDictionary = users.value as? [String: Any] else { continue }
                    //print(profileDictionary)
                    //print(profileDictionary["email"])
                    //print(profileDictionary)
                    //self.friendsArray.append(profileDictionary)
                    friendsService.friendsList.append(profileDictionary)
                }
            
            }
            )
        print(friendsService.friendsList)
    }
}
