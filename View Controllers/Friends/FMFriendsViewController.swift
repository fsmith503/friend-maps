//
//  FMFriendsViewController.swift
//  Friend Maps
//
//  Created by  Franklin Smith on 10/12/18.
//  Copyright © 2018  Franklin Smith. All rights reserved.
//

import UIKit
import  Firebase
import  FirebaseDatabase

class User1: NSObject {
    var name: String?
    var email: String?
    var latitude: Double?
    var longitude: Double?
    var imageURL: String?
}

class FMFriendsViewController: UIViewController {
    
// MARK : - Properties
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var friendsLabel: UILabel!
    @IBOutlet var addFriendsButton: UIButton!
    @IBOutlet public weak var tableView: UITableView!
    
    var ref: DatabaseReference!
    var user1Array: Array<User1> = []
    var friendsArray: Array<AnyObject> = []
    
// MARK : - Lifecycle Methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.friendsArray = []
        self.user1Array = []
        //DispatchQueue.main.async {
            //self.getFriends()
            //self.fetchUsers()
            //self.tableView.reloadData()
            //viewDidLoad()
            //print("reload data called")
        //}
        ref = Database.database().reference()
        
        // layout
        view.backgroundColor = UIColor.MapFriends.green
        friendsLabel.font = UIFont.MapFriendsFonts.fontLargeTitle
        friendsLabel.textColor = UIColor.white
        backButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        backButton.layer.cornerRadius = 5
        backButton.layer.borderWidth = 1
        backButton.layer.borderColor = UIColor.white.cgColor
        addFriendsButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        addFriendsButton.layer.cornerRadius = 5
        addFriendsButton.layer.borderWidth = 1
        addFriendsButton.layer.borderColor = UIColor.white.cgColor
        
        //table view
        tableView.dataSource = self
        tableView.delegate = self
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.tableView.layoutMargins = UIEdgeInsets.zero;
        
        //getFriends
        getFriends()
        
        //Retrieve users from database
        fetchUsers()
        
        for user in user1Array {
            print(user.name ?? "No Name")
        }
        
        // register cell class
        let nib = UINib(nibName: "FriendsTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cellId")
    }
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        ref = Database.database().reference()
//
//        // layout
//        view.backgroundColor = UIColor.MapFriends.green
//        friendsLabel.font = UIFont.MapFriendsFonts.fontLargeTitle
//        friendsLabel.textColor = UIColor.white
//        backButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
//        backButton.layer.cornerRadius = 5
//        backButton.layer.borderWidth = 1
//        backButton.layer.borderColor = UIColor.white.cgColor
//        addFriendsButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
//        addFriendsButton.layer.cornerRadius = 5
//        addFriendsButton.layer.borderWidth = 1
//        addFriendsButton.layer.borderColor = UIColor.white.cgColor
//
//        //table view
//        tableView.dataSource = self
//        tableView.delegate = self
//        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
//        self.tableView.layoutMargins = UIEdgeInsets.zero;
//
//        //getFriends
//        getFriends()
//
//        //Retrieve users from database
//        fetchUsers()
//
//        for user in user1Array {
//            print(user.name ?? "No Name")
//        }
//
//        // register cell class
//        let nib = UINib(nibName: "FriendsTableViewCell", bundle: nil)
//        tableView.register(nib, forCellReuseIdentifier: "cellId")
//    }
    
    // MARK: - get friends list
    func getFriends(){
        DispatchQueue.main.async {
        self.friendsArray = []
        self.ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        self.ref.child("users").child("profile").child(userID!).child("friendsList").observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            if(value != nil){
                self.friendsArray.append(value!)
            }
            else {
                print("value is nil")
            }
        })
        }
    }
    
// MARK : - Friend List Method
    
    func fetchUsers() {
        ref.child("users").observeSingleEvent(of: .childAdded, with: { (snapshot) in
            
            for users in snapshot.children.allObjects as! [DataSnapshot] {
                if let profileDictionary = users.value as? [String: Any] {
                    let testerArray = Array(self.friendsArray[0].allValues)
                    for item in testerArray{
                        if String(describing: item) == profileDictionary["email"] as? String{
                            let user1 = User1()
                            user1.name = profileDictionary["username"] as? String
                            user1.email = profileDictionary["email"] as? String
                            user1.latitude = profileDictionary["latitude"] as? Double
                            user1.longitude = profileDictionary["longitude"] as? Double
                            user1.imageURL = profileDictionary["photoURL"] as? String
                            self.user1Array.append(user1)
                        }
                        else {
                        }
                    }
                    self.getFriends()
                }
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
    }
    
// MARK : - Button Action Methods
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func addFriendsButtonPressed(_sender: Any){
        let vc = FMAddFriendsViewController(nibName: "FMAddFriendsViewController", bundle: nil)
        self.present(vc, animated: true, completion: nil)
    }
}

extension FMFriendsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // set up cell
        var cell: FriendsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cellId") as? FriendsTableViewCell
        if cell == nil {
            cell = FriendsTableViewCell()
        }
        
        
        // grab image
        if let profileImageURL = user1Array[indexPath.row].imageURL {
            let url = URL(string: profileImageURL)
            URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                
                //download hit an error
                if error != nil {
                    print(error!)
                    return
                }
                
                DispatchQueue.main.async {
                    cell?.imageView?.image = UIImage(data: data!)
                }
            }).resume()
        }
        
        cell?.textLabel?.text = user1Array[indexPath.row].name
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     return 80
     }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return user1Array.count
    }
}
