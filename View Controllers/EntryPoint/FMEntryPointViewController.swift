//
//  FMEntryPointViewController.swift
//  Friend Maps
//
//  Created by  Franklin Smith on 10/8/18.
//  Copyright © 2018  Franklin Smith. All rights reserved.
//

import UIKit
import Firebase

class FMEntryPointViewController: UIViewController
{
    
// MARK: - Properties
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var logoImage: UIImageView!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var signupButton: UIButton!
    @IBOutlet var kaitlynLabel: UILabel!
    @IBOutlet var franklinLabel: UILabel!
    
    
    
// MARK: - Lifecycle methods

    override func viewDidLoad()
    {
        super.viewDidLoad()
        view.backgroundColor = UIColor.MapFriends.green
        setupEntryPage()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        if let user = Auth.auth().currentUser{
            let vc = FMProfileViewController(nibName: "FMProfileViewController", bundle: nil)
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
// MARK: - Logic Functions
    
    func setupEntryPage(){
        titleLabel.font = UIFont.MapFriendsFonts.fontLargeTitle
        titleLabel.textColor = UIColor.white
        logoImage.image = UIImage(named: "Icon-1024")!
        loginButton.backgroundColor = UIColor.red
        loginButton.titleLabel?.textColor = UIColor.white
        loginButton.titleLabel?.font = UIFont.MapFriendsFonts.fontLargeTitle
        signupButton.backgroundColor = UIColor.MapFriends.buttonBlue
        signupButton.titleLabel?.textColor = UIColor.white
        signupButton.titleLabel?.font = UIFont.MapFriendsFonts.fontLargeTitle
        kaitlynLabel.font = UIFont.MapFriendsFonts.fontHeadline
        kaitlynLabel.textColor = UIColor.white
        franklinLabel.font = UIFont.MapFriendsFonts.fontHeadline
        franklinLabel.textColor = UIColor.white
    }
    

// MARK: - Button Actions

    @IBAction func buttonPressedSignup()
    {
        let vc = FMSignupViewController(nibName: "FMSignupViewController", bundle: nil)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func buttonPressedLogin()
    {
        let vc = FMLoginViewController(nibName: "FMLoginViewController", bundle: nil)
        self.present(vc, animated: true, completion: nil)
    }

}


