//
//  AddFriendsViewController.swift
//  Friend Maps
//
//  Created by  Franklin Smith on 10/12/18.
//  Copyright © 2018  Franklin Smith. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class User: NSObject {
    var name: String?
    var email: String?
    var latitude: String?
    var longitude: String?
    var imageURL: String?
}

class FMAddFriendsViewController: UIViewController {

// MARK : - Properties
    @IBOutlet var backButton: UIButton!
    @IBOutlet var addfriendsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var friendsArray = [NSDictionary]()
    var userArray: Array<User> = []
    var filteredArray: Array <User> = []
    var ref: DatabaseReference!
   
// MARK : - Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        // layout
        searchBar.tintColor = UIColor.MapFriends.green
        searchBar.backgroundColor = UIColor.MapFriends.green
        view.backgroundColor = UIColor.MapFriends.green
        addfriendsLabel.font = UIFont.MapFriendsFonts.fontLargeTitle
        addfriendsLabel.textColor = UIColor.white
        backButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        backButton.layer.cornerRadius = 5
        backButton.layer.borderWidth = 1
        backButton.layer.borderColor = UIColor.white.cgColor
        
        // table view
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.tableView.layoutMargins = UIEdgeInsets.zero;
        
        // search bar
        searchBar.delegate = self
        
        // getting the friends
        getFriends()
        
        //Retrieve users from database
        fetchUsers()
        filteredArray = userArray
        
        // register cell class
        let nib = UINib(nibName: "FriendsTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cellId")
    }
    
// MARK: - get friends list
    func getFriends(){
        self.friendsArray = []
        self.ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        self.ref.child("users").child("profile").child(userID!).child("friendsList").observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            if(value != nil){
                self.friendsArray.append(value!)
            }
            else {
                print("value is nil")
            }
        })
    }

// MARK: Fetch Users
    func fetchUsers() {
        ref.child("users").observeSingleEvent(of: .childAdded, with: { (snapshot) in
            for users in snapshot.children.allObjects as! [DataSnapshot] {
                if let profileDictionary = users.value as? [String: AnyObject] {
                    let user = User()
                    user.name = profileDictionary["username"] as? String
                    user.email = profileDictionary["email"] as? String
                    user.latitude = profileDictionary["latitude"] as? String
                    user.longitude = profileDictionary["longitude"] as? String
                    user.imageURL = profileDictionary["photoURL"] as? String
                    self.userArray.append(user)
                }
            }
            // run on main thread instead of background thread
            DispatchQueue.main.async {
                self.filteredArray = self.userArray
                self.tableView.reloadData()
            }
        })
    }
    
    
// MARK : - Button Action Methods/Users/kaitlynwright/Desktop/FriendMaps/friend-maps/View Controllers/Add Friends/FMAddFriendsViewController.swift
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true)
        //FMFriendsViewController.tableView.reloadData()
    }
}

extension FMAddFriendsViewController: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // set up cell
        var cell: FriendsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cellId") as? FriendsTableViewCell
        if cell == nil {
            cell = FriendsTableViewCell()
        }
        
        // grab image
        if let profileImageURL = filteredArray[indexPath.row].imageURL {
            let url = URL(string: profileImageURL)
            URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                
                //download hit an error
                if error != nil {
                    print(error!)
                    return
                }
                
                DispatchQueue.main.async {
                    cell?.imageView?.image = UIImage(data: data!)
                }
            }).resume()
        }
        
        cell?.textLabel?.text = filteredArray[indexPath.row].name
        cell?.selectionStyle = UITableViewCellSelectionStyle.gray
        return cell!
    }
    
     func tableViewDidSelectRowAtIndexPathForAddFriends(indexPath: IndexPath) {
        let friendName = filteredArray[indexPath.row].name
        let friendEmail = filteredArray[indexPath.row].email
        let alert = UIAlertController(title: "Add Friend Confirmation", message: "Would you like to add" + " " + friendName! + " " + "to your friends list?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Nope", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "Dope", style: .default, handler: { action in
            
            // get friends
            let testerArray = Array(self.friendsArray[0].allValues)
            var friendBool = false
            for item in testerArray {
                if String(describing: item) == friendEmail{
                    friendBool = true
                    break
                }
                else {
                    friendBool = false
                }
            }
                if(friendBool){
                    // friend already in friend list
                    let alert = UIAlertController(title: "Friend Already Added :)", message: friendName! + " " + "is already in your friends list!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "My Bad", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else {
                    // friend not in friend list, add friend
                    guard let uid = Auth.auth().currentUser?.uid else { return }
                    let databaseRef = Database.database().reference().child("users/profile/\(uid)").child("friendsList")
                    databaseRef.updateChildValues([String(Int.random(in: 0 ... 99999)):self.userArray[indexPath.row].email!])
                    let alert = UIAlertController(title: "Success :)", message: friendName! + " " + "was just added to your friends list!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Great", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            self.getFriends()
        }))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
            tableViewDidSelectRowAtIndexPathForAddFriends(indexPath: indexPath)
    }
    
    // search bar delegate
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredArray = searchText.isEmpty ? userArray : userArray.filter({ (user: User) -> Bool in
                
            return user.name!.range(of: searchText, options: .caseInsensitive) != nil
        })
        tableView.reloadData()
    }
    
}
