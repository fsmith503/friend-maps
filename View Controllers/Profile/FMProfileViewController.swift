//
//  FMProfileViewController.swift
//  Friend Maps
//
//  Created by  Franklin Smith on 10/8/18.
//  Copyright © 2018  Franklin Smith. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import Foundation
import MapKit

public var friendsArray : Array<Any> = []

class FMProfileViewController: UIViewController 
{
    
// MARK: - properties
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailMarker: UILabel!
    @IBOutlet weak var latitudeValue: UILabel!
    @IBOutlet weak var longitudeValue: UILabel!
    @IBOutlet weak var latitudeMarker: UILabel!
    @IBOutlet weak var longitudeMarker: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var friendsButton: UIButton!
    @IBOutlet weak var deleteAccountButton: UIButton!
    public var currentLocation: CLLocationCoordinate2D?
    private let locationManager = CLLocationManager()
    var ref: DatabaseReference!
    public var lat : String!
    public var long : String!
    
// MARK: - Lifecycle Methods
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        view.backgroundColor = UIColor.MapFriends.green
        setupProfilePage()
        setupProfileImage()
        locationManager.delegate = self
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        configureLocationServices()
        friendsTest()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    
// MARK: - Logic Functions
    
    func friendsTest(){
        ref.child("users").child("profile").observeSingleEvent(of: .value, with: { (snapshot) in
            for users in snapshot.children.allObjects as! [DataSnapshot] {
                guard let profileDictionary = users.value as? [String: Any] else { continue }
                    friendsArray.append(profileDictionary)
            }
        }
    )
    }

    
    func configureLocationServices(){
        locationManager.delegate = self
        
        let status = CLLocationManager.authorizationStatus()
        
        if status == .notDetermined {
            locationManager.requestAlwaysAuthorization() // may change to only when in use authorization
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: locationManager)
        }
    }
    
    public func beginLocationUpdates(locationManager: CLLocationManager) {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest // can change to best for navigation
        locationManager.startUpdatingLocation()
        
    }
    
    
    func setupProfilePage(){
        view.backgroundColor = UIColor.MapFriends.green
        nameLabel.font = UIFont.MapFriendsFonts.fontLargeTitle
        nameLabel.textColor = UIColor.white
        emailLabel.font = UIFont.MapFriendsFonts.fontHeadline
        emailLabel.textColor = UIColor.white
        latitudeValue.font = UIFont.MapFriendsFonts.fontHeadline
        latitudeValue.textColor = UIColor.white
        longitudeValue.font = UIFont.MapFriendsFonts.fontHeadline
        longitudeValue.textColor = UIColor.white
        emailMarker.font = UIFont.MapFriendsFonts.fontHeadline
        emailMarker.textColor = UIColor.white
        latitudeMarker.textColor = UIColor.white
        latitudeMarker.font = UIFont.MapFriendsFonts.fontHeadline
        latitudeMarker.text = "Latitude:"
        longitudeMarker.textColor = UIColor.white
        longitudeMarker.font = UIFont.MapFriendsFonts.fontHeadline
        longitudeMarker.text = "Longitude:"
        mapButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        mapButton.layer.cornerRadius = 5
        mapButton.layer.borderWidth = 1
        mapButton.layer.borderColor = UIColor.white.cgColor
        deleteAccountButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        //deleteAccountButton.titleLabel?.textColor = UIColor.red
        deleteAccountButton.setTitleColor(UIColor.red, for: UIControlState.normal)
        deleteAccountButton.layer.cornerRadius = 5
        deleteAccountButton.layer.borderWidth = 1
        deleteAccountButton.layer.borderColor = UIColor.white.cgColor
        deleteAccountButton.isHidden = true
        logoutButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        logoutButton.layer.cornerRadius = 5
        logoutButton.layer.borderWidth = 1
        logoutButton.layer.borderColor = UIColor.white.cgColor
        friendsButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        friendsButton.layer.cornerRadius = 5
        friendsButton.layer.borderWidth = 1
        friendsButton.layer.borderColor = UIColor.white.cgColor
        self.nameLabel.text = Auth.auth().currentUser?.displayName
        self.emailLabel.text = Auth.auth().currentUser?.email
        
        ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        ref.child("users").child("profile").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            if(value != nil){
                let latitude = value?["latitude"] as? String ?? ""
                let longitude = value?["longitude"] as? String ?? ""
                self.latitudeValue.text = latitude
                self.longitudeValue.text = longitude
            }
            else {
                print("value is nil")
            }
        }) { (error) in
            print(error.localizedDescription)
        }

    }
    
    func setupProfileImage(){
        if (Auth.auth().currentUser?.photoURL == nil) {
            self.profileImage.image = #imageLiteral(resourceName: "signuplogo")
            self.profileImage.clipsToBounds = true
            self.profileImage.layer.borderWidth = 3.0
            self.profileImage.layer.borderColor = UIColor.white.cgColor
        }
        else {
            ImageService.getImage(withURL: (Auth.auth().currentUser?.photoURL!)!){ image in
                self.profileImage.image = image
                self.profileImage.clipsToBounds = true
                self.profileImage.layer.borderWidth = 3.0
                self.profileImage.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
 

// MARK: - Button Actions

    @IBAction func buttonPressedMapPage()
    {
        let vc = FMMapViewController(nibName: "FMMapViewController", bundle: nil)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func buttonPressedBack()
    {
        //try! Auth.auth().signOut()
        //self.dismiss(animated: true)
        let alert = UIAlertController(title: "Logout?", message: "Would you like to logout of Friend Maps?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "Ya", style: .default, handler: { action in
            try! Auth.auth().signOut()
            self.dismiss(animated: true)
        }))
    }
    
    @IBAction func buttonPressedFriends(_ sender: Any)
    {
        let vc = FMFriendsViewController(nibName: "FMFriendsViewController", bundle: nil)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func buttonPresssedDeleteAccount(){
        let alert = UIAlertController(title: "Delete Account :(", message: "If you would like to delete your account, please email Friend Maps administrators at kaitlynw@uoregon.edu or fsmith4@uoregon.edu with your login credentials and we will gladly remove your account from the database.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Gotcha", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}


// MARK: - Delegate extension
extension FMProfileViewController: CLLocationManagerDelegate, MKMapViewDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did get latest location")
        
        guard let latestLocation = locations.first else { return }
        
        if currentLocation == nil {
        }
        
        currentLocation = latestLocation.coordinate
        if (currentLocation != nil){
            lat = String(describing: currentLocation!.latitude)
            long = String(describing: currentLocation!.longitude)
            let destination = Database.database().reference().child("users").child("profile").child((Auth.auth().currentUser?.uid)!)
            destination.updateChildValues(["latitude": lat])
            destination.updateChildValues(["longitude": long])
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: manager)
        }
    }
}


