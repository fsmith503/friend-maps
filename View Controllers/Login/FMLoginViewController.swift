//
//  FMLoginViewController.swift
//  Friend Maps
//
//  Created by  Franklin Smith on 10/9/18.
//  Copyright © 2018  Franklin Smith. All rights reserved.
//

import UIKit
import Firebase

class FMLoginViewController: UIViewController {

// MARK: - Properties
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet var loginLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var passwordLabel: UILabel!
    @IBOutlet var continueButton: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var forgotPassButton: UIButton!
    
// MARK: - Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.isHidden = true
        setupLoginPage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    

// MARK: - Logic Methods
    
    func setupLoginPage(){
        view.backgroundColor = UIColor.MapFriends.green
        loginLabel.font = UIFont.MapFriendsFonts.fontLargeTitle
        loginLabel.textColor = UIColor.white
        emailLabel.font = UIFont.MapFriendsFonts.fontHeadline
        passwordLabel.font = UIFont.MapFriendsFonts.fontHeadline
        continueButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        continueButton.layer.cornerRadius = 5
        continueButton.layer.borderWidth = 1
        continueButton.layer.borderColor = UIColor.white.cgColor
        forgotPassButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        forgotPassButton.layer.cornerRadius = 5
        forgotPassButton.layer.borderWidth = 1
        forgotPassButton.layer.borderColor = UIColor.white.cgColor
        backButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        backButton.layer.cornerRadius = 5
        backButton.layer.borderWidth = 1
        backButton.layer.borderColor = UIColor.white.cgColor
        self.hideKeyboard1(#selector(self.dismissKeyboard))
    }
    
 @objc func dismissKeyboard() {
        view.endEditing(true)
        // do aditional stuff
    }
    
    @objc func handleSignIn(){
        guard let email = emailField.text else { return }
        guard let pass = passwordField.text else { return }
        
        Auth.auth().signIn(withEmail: email, password: pass) { user, error in
            if error == nil && user != nil {
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                let vc = FMProfileViewController(nibName: "FMProfileViewController", bundle: nil)
                self.present(vc, animated: true, completion: nil)
                self.emailField.text = ""
                self.passwordField.text = ""
            } else {
                print("Error loggin in: \(error!.localizedDescription)")
                let alert = UIAlertController(title: "Error Logging In", message: "Error loggin in: \(error!.localizedDescription)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
// MARK: - Button Actions
    
    @IBAction func buttonPressedBack()
    {
        self.dismiss(animated: true)
    }
    
    @IBAction func buttonPressedContinue()
    {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        handleSignIn()
    }
    
    @IBAction func buttonPressedForgot(){
        let alert = UIAlertController(title: "Not Good :(", message: "Keep guessing or create A New Account Tho", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension UIViewController {
    func hideKeyboard1(_ selector: Selector) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: selector)
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
}
