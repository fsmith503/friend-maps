//
//  FMMapViewController.swift
//  Friend Maps
//
//  Created by Kaitlyn Wright on 10/9/18.
//  Copyright © 2018  Franklin Smith. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import FirebaseDatabase
import CoreLocation
import LyftSDK
import MessageUI

// MARK: MapView Controller
class FMMapViewController: UIViewController , MFMailComposeViewControllerDelegate {
    
// MARK: - Properties
    private let locationManager = CLLocationManager()
    public var currentLocation: CLLocationCoordinate2D?
    public var midpointCoordinate: CLLocationCoordinate2D?
    public var localBusinesses: [MKMapItem] = []
    public var userArray: [User] = []
    var friendsArray: Array<AnyObject> = []
    var ref: DatabaseReference!
    var emailrecip = ""
    var emailsubject = ""
    var emailbody = ""
    var friendEmail: String?
    var friendName: String?
    var friendCoordinate: CLLocationCoordinate2D?
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var mapTitleLabel: UILabel!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet weak var recenterButton: UIBarButtonItem!
    @IBOutlet weak var resetButton: UIBarButtonItem!
    @IBOutlet weak var btnLyft: LyftButton!
    @IBOutlet var bottomBar: UIToolbar!
    @IBOutlet var mailButton: UIButton!
    
// MARK : - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        ref = Database.database().reference()
        
        view.backgroundColor = UIColor.MapFriends.green
        backButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        backButton.layer.cornerRadius = 5
        backButton.layer.borderWidth = 1
        backButton.layer.borderColor = UIColor.white.cgColor
        mapTitleLabel.font = UIFont.MapFriendsFonts.fontLargeTitle
        mapTitleLabel.textColor = UIColor.white
        bottomBar.barTintColor = UIColor.MapFriends.green
        recenterButton.tintColor = UIColor.white
        resetButton.tintColor = UIColor.white
        
        // get user profile image
        if (Auth.auth().currentUser?.photoURL == nil) {
            self.profileImage.image = #imageLiteral(resourceName: "signuplogo")
        }
        else {
            ImageService.getImage(withURL: (Auth.auth().currentUser?.photoURL!)!){ image in
                self.profileImage.image = image
            }
        }
        confirgureLocationServices()
        getFriends()
        addFriendAnnotations()
        
        btnLyft.isHidden = true
        mailButton.isHidden = true

    }
    
// MARK: - Email Methods
    
    func configureMailController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([self.emailrecip])
        mailComposerVC.setSubject(self.emailsubject)
        mailComposerVC.setMessageBody(self.emailbody, isHTML: false)
        return mailComposerVC
    }
    
    func showMailError() {
        let sendMailErrorAlert = UIAlertController(title: "Could not send email", message: "Your device could not send email", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Ok", style: .default, handler: nil)
        sendMailErrorAlert.addAction(dismiss)
        self.present(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
// MARK: - get friends list
    func getFriends(){
        DispatchQueue.main.async {
            self.friendsArray = []
            let userID = Auth.auth().currentUser?.uid
            self.ref.child("users").child("profile").child(userID!).child("friendsList").observeSingleEvent(of: .value, with: { (snapshot) in
                let value = snapshot.value as? NSDictionary
                if(value != nil){
                    self.friendsArray.append(value!)
                }
                else {
                    print("value is nil")
                }
            })
        }
    }
    
    
// MARK: - Location Methods
    private func confirgureLocationServices() {
        locationManager.delegate = self
        
        let status = CLLocationManager.authorizationStatus()
        
        if status == .notDetermined {
            locationManager.requestAlwaysAuthorization() // may change to only when in use authorization
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: locationManager)
        }
    }
    
    private func beginLocationUpdates(locationManager: CLLocationManager) {
        mapView.showsUserLocation = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest // can change to best for navigation
        locationManager.startUpdatingLocation()
    }
    
    private func zoomToLatestLocation(with coordinate: CLLocationCoordinate2D) {
        //print("zoomToLatestLocation called")
        let zoomRegion = MKCoordinateRegionMakeWithDistance(coordinate, 50000, 50000)
        mapView.setRegion(zoomRegion, animated: true)
    }
    
    private func locationServicesChecker() {
        locationManager.delegate = self
        
        let status = CLLocationManager.authorizationStatus()
        
        if status == .denied {
            //do alert and return
            //alert
            let alert = UIAlertController(title: "Location Services Disabled", message: "Please Enable Location Services for Friend Maps to use this feature.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            locationManager.requestAlwaysAuthorization()
            return
            // may change to only when in use authorization
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: locationManager)
        }
    }
    
    
// MARK: - Local Business Search
    func updateLocalBusinesses() {
        // remove previous search annotations
        let midpointAnnotationList = mapView.annotations.filter { $0.isKind(of: MidpointAnnotation.self)}
        
        var centerCoordinate = currentLocation
        if midpointAnnotationList.count != 0 {
            centerCoordinate = midpointAnnotationList[0].coordinate
        }
        
        if localBusinesses != [] {
            let annotationsToRemove = mapView.annotations.filter { $0.isKind(of: CustomAnnotation.self) }
            mapView.removeAnnotations(annotationsToRemove)
        }
        
        // start new search
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchTextField.text
        request.region = mapView.region
        
        let search = MKLocalSearch(request: request)
        search.start { response, _ in
            guard let response = response else {
                return
            }
            
            // retrieve search results
            self.localBusinesses = response.mapItems
            
            // check if businesses are within region
            var businessesInView: [Bool] = []
            let centerAnnotation = MidpointAnnotation(coordinate: centerCoordinate!, title: nil, subtitle: nil)
            var businessAnnotations: [MKAnnotation] = [centerAnnotation]
            
            for business in self.localBusinesses {
                let point = MKMapPointForCoordinate(business.placemark.coordinate)
                let businessInView = MKMapRectContainsPoint(self.mapView.visibleMapRect, point)
                businessesInView.append(businessInView)
            }
            
            // if there are businesses within region, add them to map
            for bool in businessesInView {
                if bool {
                    for business in self.localBusinesses {
                        let annotation = CustomAnnotation(coordinate: business.placemark.coordinate, title: business.name, subtitle: business.placemark.title)
                        businessAnnotations.append(annotation)
                        
                        self.mapView.addAnnotation(annotation)
                    }
                    
                    self.mapView.showAnnotations(businessAnnotations, animated: true)
                    self.btnLyft.isHidden = false
                    self.mapView.removeAnnotation(centerAnnotation)
                    return
                }
            }
            
            // if no businesses within region, present message
            let alert = UIAlertController(title: "No businesses with keyword " + self.searchTextField.text! + " within range", message: "Expand map region or use different keyword.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }

    func addFriendAnnotations() {
        ref.child("users").observeSingleEvent(of: .childAdded, with: { (snapshot) in
            DispatchQueue.main.async {
                for users in snapshot.children.allObjects as! [DataSnapshot] {
                    if let profileDictionary = users.value as? [String: AnyObject] {
                        let testerArray = Array(self.friendsArray[0].allValues)
                        
                        for item in testerArray {
                            if String(describing: item) == profileDictionary["email"] as? String {
                                let user = User()
                                user.name = profileDictionary["username"] as? String
                                user.email = profileDictionary["email"] as? String
                                user.latitude = profileDictionary["latitude"] as? String
                                user.longitude = profileDictionary["longitude"] as? String
                                user.imageURL = profileDictionary["photoURL"] as? String
                                self.userArray.append(user)
                            }
                        }
                        self.getFriends()
                    }
                }
            
                // add user to map, zoom to include all annotations
                var allAnnMapRect = MKMapRectNull
                
                for user in self.userArray {
                    let coordinate = CLLocationCoordinate2D(latitude: Double(user.latitude!)!, longitude: Double(user.longitude!)!)
                
                    let annotation = FriendAnnotation(coordinate: coordinate, title: user.name, subtitle: user.email)
                    annotation.imageURL = user.imageURL
                    annotation.email = user.email
                
                    //zoom to include annotations
                    let annMapPoint = MKMapPointForCoordinate(annotation.coordinate)
                    let annMapRect = MKMapRectMake(annMapPoint.x, annMapPoint.y, 1, 1)
                    allAnnMapRect = MKMapRectUnion(allAnnMapRect, annMapRect)
                    
                    if user.email != Auth.auth().currentUser?.email! {
                        self.mapView.addAnnotation(annotation)
                    }
                }
                
                let edgeInset = UIEdgeInsetsMake(75, 75, 75, 75)
                self.mapView.setVisibleMapRect(allAnnMapRect, edgePadding: edgeInset, animated: true)
            }
        })
    }
    

// MARK: - Button Actions
    @IBAction func buttonPressedBack() {
        self.dismiss(animated: true)
    }
    
    @IBAction func sendEmail(_ sender: Any) {
        let mailComposeViewController = configureMailController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            showMailError()
        }
    }
    
    @IBAction func textFieldEditingComplete(_ sender: Any) {
        locationManager.delegate = self
        let status = CLLocationManager.authorizationStatus()
        if status == .denied || status == .notDetermined {
            ///do alert and return
            //alert
            let alert = UIAlertController(title: "Location Services Disabled", message: "Please Enable Location Services in Settings->Friend Maps to use the search feature. ", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default){ UIAlertAction in
            })
            self.present(alert, animated: true)
            return
        }
        else {
        let midpointAnnotationList = mapView.annotations.filter { $0.isKind(of: MidpointAnnotation.self)}
        if midpointAnnotationList.count != 0 {
            let midpointCoordinate = midpointAnnotationList[0].coordinate
            zoomToLatestLocation(with: midpointCoordinate)
        } else {
            zoomToLatestLocation(with: currentLocation!)
        }
        updateLocalBusinesses()
        }
    }
    
    @IBAction func resetButtonPressed(_ sender: Any) {
        let annotationsToRemove = mapView.annotations.filter { !$0.isKind(of: MKUserLocation.self)  }
        mapView.removeAnnotations(annotationsToRemove)
        addFriendAnnotations()
        self.btnLyft.isHidden = true
        self.mailButton.isHidden = true
        self.friendEmail = nil
    }
    
    @IBAction func recenterButtonPressed(_ sender: Any) {
        locationManager.delegate = self
        let status = CLLocationManager.authorizationStatus()
        if status == .denied || status == .notDetermined {
            ///do alert and return
            //alert
            let alert = UIAlertController(title: "Location Services Disabled", message: "Please Enable Location Services in Settings->Friend Maps to use the recenter feature. ", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default){ UIAlertAction in
            })
            self.present(alert, animated: true)
            return
        }
        else {
            let midpointAnnotations = mapView.annotations.filter { $0.isKind(of: MidpointAnnotation.self) }
            if midpointAnnotations.count == 0 {
                zoomToLatestLocation(with: currentLocation!)
            } else {
                zoomToLatestLocation(with: midpointAnnotations[0].coordinate)
            }
            
        }
    }
    
}

// MARK: - Delegate extension
extension FMMapViewController: CLLocationManagerDelegate, MKMapViewDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("Did get latest location")
        
        guard let latestLocation = locations.first else { return }
        
        currentLocation = latestLocation.coordinate
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //called when location authorization changed by user
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: manager)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView")

        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
        }
        
        if annotation.isKind(of: FriendAnnotation.self) {
            // dequeue view if possible
            annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "friendAnnotationView")
            
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "friendAnnotationView")
            }
            
            //handle annotation
            let newAnnotation = annotation as! FriendAnnotation
            
            ImageService.getImage(withURL: NSURL(string: newAnnotation.imageURL!)! as URL){ image in
                let size = CGSize(width: 50, height: 50)
                UIGraphicsBeginImageContext(size)
                image!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: size.width + 5, height: size.height + 5))
                imageView.image = resizedImage
                imageView.layer.cornerRadius = imageView.layer.frame.size.width / 2
                imageView.layer.masksToBounds = true
                
                annotationView?.calloutOffset = CGPoint(x: 0, y: -10)
                annotationView?.frame = imageView.frame
                annotationView?.addSubview(imageView)
            }
            
            // add callout button
            let rightButton = UIButton(type: .contactAdd)
            rightButton.tag = 1
            annotationView?.canShowCallout = true
            annotationView?.rightCalloutAccessoryView = rightButton
         
        } else if annotation.isKind(of: CustomAnnotation.self) {
            annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "customAnnotationView")
            
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "customAnnotationView")
            }
            
            annotationView?.image = UIImage(named: "location_icon_green_2" )
            
            // add callout button
            if friendEmail != nil {
            let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
            rightButton.setImage(UIImage(named: "mailicon"), for: UIControlState.normal)
            rightButton.tag = 2
            annotationView?.canShowCallout = true
            annotationView?.rightCalloutAccessoryView = rightButton
            }
            
        } else if annotation.isKind(of: MidpointAnnotation.self) {
            annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "midpointAnnotationView")
            
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "midpointAnnotationView")
            }
            
            annotationView?.image = UIImage(named: "location_icon")
            
        } else if annotation === mapView.userLocation {
            ImageService.getImage(withURL: (Auth.auth().currentUser?.photoURL!)!){ image in
                let size = CGSize(width: 50, height: 50)
                UIGraphicsBeginImageContext(size)
                image!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: size.width + 5, height: size.height + 5))
                imageView.image = resizedImage
                imageView.layer.cornerRadius = imageView.layer.frame.size.width / 2
                imageView.layer.masksToBounds = true
                imageView.layer.borderWidth = 2
                imageView.layer.borderColor = UIColor.MapFriends.green.cgColor
                
                annotationView?.addSubview(imageView)
            }
        }
        
        annotationView?.canShowCallout = true
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        // called when callout button pressed
        
        if control.tag == 1 {
            
            self.locationManager.delegate = self
            let status = CLLocationManager.authorizationStatus()
            if status == .denied || status == .notDetermined {
                ///do alert and return
                //alert
                let alert = UIAlertController(title: "Location Services Disabled", message: "Please Enable Location Services in Settings->Friend Maps to use this feature. ", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                self.present(alert, animated: true)
                return
            } // end of if control.tag == 1
            else {
                //else code
            
            let alert = UIAlertController(title: "Map Connect with" + " " + (view.annotation?.title!)! + "?", message: nil, preferredStyle: UIAlertControllerStyle.alert)
            let connectAction = UIAlertAction(title: "Connect", style: UIAlertActionStyle.default) {
                UIAlertAction in
                //print("Connect Clicked")
                // put location checker code
                
                // remove previous midpoint markers
                let midpointAnnotationList = mapView.annotations.filter { $0.isKind(of: MidpointAnnotation.self)}
                mapView.removeAnnotations(midpointAnnotationList)
                
                // place new midpoint marker
                self.friendName = view.annotation?.title!
                self.friendEmail = view.annotation?.subtitle!
                self.friendCoordinate = view.annotation?.coordinate

                for user in self.userArray {
                    if user.email == self.friendEmail {
                        let midpointCoordinate = self.coordinateMidpoint(coordinateList: [self.friendCoordinate!, self.currentLocation!])
                        let midpointAnnotation = MidpointAnnotation(coordinate: midpointCoordinate, title: "Your Midpoint", subtitle: String(midpointCoordinate.latitude) + " " +   String(midpointCoordinate.longitude))
                        self.mapView.addAnnotation(midpointAnnotation)
                        self.mailButton.isHidden = false
                        
                        // zoom to midpoint annotation
                        self.zoomToLatestLocation(with: midpointCoordinate)
                        
                        // save email information
                        self.emailrecip = (view.annotation?.subtitle!)!
                        self.emailsubject = "Hello " + (view.annotation?.title!)! + " would you like to meet up :)"
                        self.emailbody = "Halfway between us is \n" + "Longitude: " + String(midpointCoordinate.longitude) + "\n" + " Latitude: " + String(midpointCoordinate.latitude) + ".\n" + "Shall I search for an establishment? \n Perhaps you would like to take Lyft?"
                        
                        // put lyft code here
                        let pickup = CLLocationCoordinate2D(latitude: Double((self.currentLocation?.latitude)!), longitude: Double((self.currentLocation?.longitude)!))
                        let destination = CLLocationCoordinate2D(latitude: midpointCoordinate.latitude, longitude: midpointCoordinate.longitude)
                        
                        self.btnLyft.configure(rideKind: LyftSDK.RideKind.Standard, pickup: pickup, destination: destination)
                        self.btnLyft.isHidden = false
                    }
                }
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                print("Cancel Clicked")
            }
            
            alert.addAction(connectAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        } // end of else statment
        } // end of control.tag ==1
    
        
        
        if control.tag == 2 {
            if (friendEmail == nil){
                //print("not connected")
            }
            else {
                // save email information
                self.emailrecip = friendEmail!
                self.emailsubject = "Hello " + friendName! + " would you like to meet up :)"
                let name = view.annotation?.title!
                let address = view.annotation?.subtitle!
                self.emailbody = "There is an establishment called " + name! + " at " + address! + " that is halfway between us. Want to meet up? \n Perhaps you would like to take a Lyft?"
            
                let mailComposeViewController = configureMailController()
                if MFMailComposeViewController.canSendMail() {
                    self.present(mailComposeViewController, animated: true, completion: nil)
                } else {
                    showMailError()
                }
            }
        }// end of if control.tag == 2
    }// end of callout method
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if (view.annotation?.isKind(of: CustomAnnotation.self))! {
            let coordinate = view.annotation?.coordinate
            let pickup = CLLocationCoordinate2D(latitude: Double((self.currentLocation?.latitude)!), longitude: Double((self.currentLocation?.longitude)!))
            
            let destination = CLLocationCoordinate2D(latitude: coordinate!.latitude , longitude: coordinate!.longitude)
            self.btnLyft.configure(rideKind: LyftSDK.RideKind.Standard, pickup: pickup, destination: destination)
            
            // ride request button
        }
    }
}


// MARK: - Midpoint calculation extension
extension FMMapViewController {
    
    func degreeToRadian(angle: CLLocationDegrees) -> CGFloat {
        return CGFloat(angle) / 180.0 * CGFloat(Double.pi)
    }
    
    func radianToDegree(radian: CGFloat) -> CLLocationDegrees {
        return CLLocationDegrees(radian * CGFloat(180.0 / Double.pi))
    }
    
    private func coordinateMidpoint(coordinateList: [CLLocationCoordinate2D]) -> CLLocationCoordinate2D {
        
        var x = 0.0 as CGFloat
        var y = 0.0 as CGFloat
        var z = 0.0 as CGFloat
        
        for coordinate in coordinateList {
            let lat = degreeToRadian(angle: coordinate.latitude)
            let lon = degreeToRadian(angle: coordinate.longitude)
            
            x = x + cos(lat) * cos(lon)
            y = y + cos(lat) * sin(lon)
            z = z + sin(lat)
        }
        
        let coordCount = CGFloat(coordinateList.count)
        x = x / coordCount
        y = y / coordCount
        z = z / coordCount
        
        let resultLon = radianToDegree( radian: atan2(y ,x) )
        let resultHyp = sqrt(x*x + y*y)
        let resultLat = radianToDegree( radian: atan2(z, resultHyp) )
        
        return CLLocationCoordinate2D(latitude: resultLat, longitude: resultLon)
    }
}
