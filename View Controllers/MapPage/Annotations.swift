//
//  Annotations.swift
//  Friend Maps
//
//  Created by Kaitlyn Wright on 11/2/18.
//  Copyright © 2018  Franklin Smith. All rights reserved.
//

import Foundation
import MapKit

// MARK: Custom Annotation Classes
final class CustomAnnotation: NSObject, MKAnnotation {
    let identifier = "customAnnotation"
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        
        super.init()
    }
}

final class FriendAnnotation: NSObject, MKAnnotation {
    let identifier = "friendAnnotation"
    var email: String?
    var imageURL: String?
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        
        super.init()
    }
}

final class MidpointAnnotation: NSObject, MKAnnotation {
    let identifier = "midpointAnnotation"
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        
        super.init()
    }
}
