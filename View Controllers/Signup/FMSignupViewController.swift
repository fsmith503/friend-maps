//
//  FMSignupViewController.swift
//  Friend Maps
//
//  Created by  Franklin Smith on 10/9/18.
//  Copyright © 2018  Franklin Smith. All rights reserved.
//

import UIKit
import Firebase
import MapKit

class FMSignupViewController: UIViewController {
    
// MARK: - Properties
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var signupLabel: UILabel!
    @IBOutlet var taptoChangeLabel: UIButton!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var passwordLabel: UILabel!
    @IBOutlet var continueButton: UIButton!
    @IBOutlet var profilePhoto: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var imagePicker: UIImagePickerController!
    public var currentLocation: CLLocationCoordinate2D?
    private let locationManager = CLLocationManager()
    public var lat : String!
    public var long : String!
    
// MARK: - Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.MapFriends.green
        setUpSignUpPage()
        locationManager.delegate = self 
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        configureLocationServices()
        self.hideKeyboard(#selector(self.dismissKeyboard))
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
// MARK: Logic Helper Methods
    
    
   @objc func dismissKeyboard() {
        view.endEditing(true)
        // do aditional stuff
    }
    
    
    func configureLocationServices(){
        locationManager.delegate = self
        
        let status = CLLocationManager.authorizationStatus()
        
        if status == .notDetermined {
            locationManager.requestAlwaysAuthorization() // may change to only when in use authorization
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: locationManager)
        }
    }
    
    public func beginLocationUpdates(locationManager: CLLocationManager) {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest // can change to best for navigation
        locationManager.startUpdatingLocation()
        
    }
    

    func setUpSignUpPage(){
        taptoChangeLabel.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        taptoChangeLabel.titleLabel?.textColor = UIColor.white
        taptoChangeLabel.layer.cornerRadius = 5
        taptoChangeLabel.layer.borderWidth = 1
        taptoChangeLabel.layer.borderColor = UIColor.white.cgColor
        usernameLabel.font = UIFont.MapFriendsFonts.fontHeadline
        usernameLabel.textColor = UIColor.white
        signupLabel.font = UIFont.MapFriendsFonts.fontLargeTitle
        signupLabel.textColor = UIColor.white
        emailLabel.font = UIFont.MapFriendsFonts.fontHeadline
        emailLabel.textColor = UIColor.white
        passwordLabel.font = UIFont.MapFriendsFonts.fontHeadline
        passwordLabel.textColor = UIColor.white
        //passwordField.isSecureTextEntry = true
        continueButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        continueButton.titleLabel?.textColor = UIColor.white
        continueButton.layer.cornerRadius = 5
        continueButton.layer.borderWidth = 1
        continueButton.layer.borderColor = UIColor.white.cgColor
        backButton.titleLabel?.font = UIFont.MapFriendsFonts.fontHeadline
        backButton.layer.cornerRadius = 5
        backButton.layer.borderWidth = 1
        backButton.layer.borderColor = UIColor.white.cgColor
        profilePhoto.image = #imageLiteral(resourceName: "signuplogo")
        profilePhoto.layer.cornerRadius = 5
        activityIndicator.isHidden = true
    }
    
    func selectImage(){
        imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
    }
    
    @objc func handleSignUp()
    {
        guard let username = usernameField.text else { return }
        guard let email = emailField.text else { return }
        guard let pass = passwordField.text else { return }
        guard let image = profileImage.image else { return }
        
        Auth.auth().createUser(withEmail: email, password: pass) { user, error in
            if error == nil && user != nil {
                print("User created!")
            // 1. Upload the profile image to Firebase Storage
                
                self.uploadProfileImage(image) { url in
                    
                    if url != nil {
                        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                        changeRequest?.displayName = username
                        changeRequest?.photoURL = url
                        
                        changeRequest?.commitChanges { error in
                            if error == nil {
                                print("User display name changed!")
                                
                                self.saveProfile(username: username, email: email, profileImageURL: url!) { success in
                                    if success {
                                        print("Saving Image :)")
                                        self.activityIndicator.isHidden = true
                                        self.activityIndicator.stopAnimating()
                                        self.usernameField.text = ""
                                        self.emailField.text = ""
                                        self.passwordField.text = ""
                                        let vc = FMProfileViewController()
                                        self.present(vc, animated:  true)
                                    }
                                }
                                
                            } else {
                                print("Error Signing Up: \(error!.localizedDescription)")
                                let alert = UIAlertController(title: "Error Signing Up", message: "Error Signing Up: \(error!.localizedDescription)", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                self.activityIndicator.isHidden = true
                                self.activityIndicator.stopAnimating()
                            }
                        }
                    } else {
                        // Error unable to upload profile image
                        print("Error Signing Up: \(error!.localizedDescription)")
                        let alert = UIAlertController(title: "Error Signing Up", message: "Error Signing Up: \(error!.localizedDescription)", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                    }
                    
                }
                
            } else {
                print("Error Signing Up: \(error!.localizedDescription)")
                let alert = UIAlertController(title: "Error Signing Up", message: "Error Signing Up: \(error!.localizedDescription)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }
        
    }
    
    func uploadProfileImage(_ image:UIImage, completion: @escaping ((_ url:URL?)->())) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let storageRef = Storage.storage().reference().child("user/\(uid)")

        guard let imageData = UIImageJPEGRepresentation(image, 0.75) else { return }


        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"

        storageRef.putData(imageData, metadata: metaData) { metaData, error in
            if error == nil, metaData != nil {
                storageRef.downloadURL {url, error in
                    completion(url)
                    // success!
                    print("uploaded profile image")
            }
            } else {
                // failed
                print("failed to upload profile image")
                completion(nil)
            }
        }
    }

    func saveProfile(username: String, email: String, profileImageURL:URL, completion: @escaping ((_ success:Bool)->())){
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let databaseRef = Database.database().reference().child("users/profile/\(uid)")
        
        let userObject = [
            "username": username,
            "photoURL": profileImageURL.absoluteString,
            "email": email,
            "latitude" : lat,
            "longitude" : long,
            "friendsList" : ""
            
        ] as [String:Any]
        
        databaseRef.setValue(userObject) { error, ref in
            completion(error == nil)
            print("User Saved")
        }
        
        guard let value = Auth.auth().currentUser?.uid else { return }
        let databaseRefvalue = Database.database().reference().child("users/profile/\(value)").child("friendsList")
        databaseRefvalue.updateChildValues([String(Int.random(in: 0 ... 99999)): email])
        
    }
    
// MARK: - Button Actions
    
    @IBAction func buttonPressedBack()
    {
        self.dismiss(animated: true)
    }
    
    @IBAction func buttonPressedContinue()
    {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        handleSignUp()
    }
    
    @IBAction func buttonPressedChangeImage()
    {
        selectImage()
        self.present(imagePicker, animated: true, completion: nil)
    }
}
    
// MARK: - Extension

extension FMSignupViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:
        [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.profileImage.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
}


// MARK: - Delegate extension
extension FMSignupViewController: CLLocationManagerDelegate, MKMapViewDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did get latest location")
        
        guard let latestLocation = locations.first else { return }
        
        if currentLocation == nil {
        }
        
        currentLocation = latestLocation.coordinate
        if (currentLocation != nil){
            lat = String(describing: currentLocation!.latitude)
            long = String(describing: currentLocation!.longitude)
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: manager)
        }
    }
}

extension UIViewController {
    func hideKeyboard(_ selector: Selector) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: selector)
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
}



