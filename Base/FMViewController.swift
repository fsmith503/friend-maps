//
//  ViewController.swift
//  Friend Maps
//
//  Created by  Franklin Smith on 10/7/18.
//  Copyright © 2018  Franklin Smith. All rights reserved.
//

import UIKit
import Firebase

let statusBarHeight: CGFloat = 20

class FMViewController: UIViewController {

// Mark: - LifeCycleMethods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle
        {
        return .default
    }


}

