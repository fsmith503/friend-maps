//
//  UIColorExtension.swift
//  Friend Maps
//
//  Created by  Franklin Smith on 10/11/18.
//  Copyright © 2018  Franklin Smith. All rights reserved.
//

import Foundation
import UIKit


extension UIColor
    {
    struct MapFriends
    {
        static let green = UIColor(hex: "#00853F")
        static let lightGray = UIColor(hex: "EDEDED")
        static let lightTan = UIColor(hex: "C7BDA0")
        static let offWhite = UIColor(hex: "F2F2F2")
        static let rust = UIColor(hex: "AD3C3C")
        static let sageGreen = UIColor(hex: "738D6F")
        static let scarlet = UIColor(hex: "E31837")
        static let white = UIColor(hex: "FFFFFF")
        static let buttonBlue = UIColor(hex: "0E6AC7")
    }
}


